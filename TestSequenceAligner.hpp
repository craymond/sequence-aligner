#pragma once
#include "SequenceAligner.hpp"
#include <sstream>

class TestSequenceAligner 
{

    inline static unsigned int passed=0;
    inline static unsigned int failed=0;
    inline static unsigned int num_test=0;
    inline static std::ostringstream log;
    static std::vector<std::string> split(const std::string& s)
    {
        std::vector<std::string> res;
        std::istringstream in(s);
        std::string tmp;
        while(in>>tmp)
            res.push_back(std::move(tmp));
        return res;
    }
    static void assertTrue(const bool res)
    {
        ++num_test;
        log << "Test "<<num_test;
        if(res)
        {
            log<<" passed\n";
            ++passed;
        }
        else
        {
            log<<" failed\n";
            ++failed;
        }
    }
    template<typename T>
    static void assertEquals(const T un,const T deux,const T precision)
    {
       assertTrue(un>=deux-precision && un <=deux+precision);
    }

	void testAlignListOfStringListOfString() const
    {
		log<<"\n-------Test sentences-------\n";
        num_test=0;
        SequenceAligner werEval;
		std::vector<SequenceAligner::Alignment> alignments;
		
		// Reference alignments and stats created with the NIST sclite tool, default settings
		alignments.push_back(werEval.align(split("the quick brown cow jumped over the moon"),split("quick brown cows jumped way over the moon dude")));
		alignments.push_back(werEval.align(split("the quick brown cow jumped over the moon"), split("quick brown over the moon dude")));
		alignments.push_back(werEval.align(split("0 1 1 0 0 1 0 0 0 1 1 1 0 0 0 0 1 1 1 0 1 1 0 1 0 1 0 1 1 1 0 0 0 0 1 1 1 0 0 1 0 1 1 0 1 0 1 1 0 1 0 0 0 1 0 1 0 0 1 0 1 0 1 1 1 0 1 1 0 0 1 1 1 0 0 1 1 1 0 0 1 0 1 1 1 1 1 0 1 0 1 0 0 1 0 0 1 1 0 1"), split("1 0 1 1 0 1 1 1 1 0 0 0 0 0 1 0 0 0 1 1 0 0 1 1 1 0 0 0 0 0 1 0 0 1 0 1 1 0 0 1 1 0 0 1 1 1 1 0 0 0 1 0 1 1 0 0 1 1 1 0 1 0 0 0 1 0 0 1 1 0 0 1 0 1 1 1 1 1 0 0 1 0 0 0 1 1 1 0 1 1 1 0 1 0 1 1 1 0 0 1")));
		
		SequenceAligner::SummaryStatistics ss(alignments);
		assertTrue(ss.getNumSentences() == 3);
		assertTrue(ss.getNumReferenceWords() == 116);
		assertTrue(ss.getNumHypothesisWords() == 115);
		assertEquals(0.776f, ss.getCorrectRate(), 0.001f);
		assertEquals(0.103f, ss.getSubstitutionRate(), 0.001f);
		assertEquals(0.121f, ss.getDeletionRate(), 0.001f);
		assertEquals(0.112f, ss.getInsertionRate(), 0.001f);
		assertEquals(0.336f, ss.getWordErrorRate(), 0.001f);
		assertEquals(1.000f, ss.getSentenceErrorRate(), 0.001f);
	}

	void testAlignStringArrayStringArray() const{
		log<<"\n-------Test string arrays-------\n";
        num_test=0;
        
        SequenceAligner werEval;
		{
		auto a = werEval.align(std::vector<std::string>(), std::vector<std::string>());
		assertTrue(a.reference==std::vector<std::string>());
		assertTrue(a.hypothesis==std::vector<std::string>());
		assertTrue(a.getHypothesisLength() == 0);
		assertTrue(a.getReferenceLength() == 0);
		assertTrue(a.getHypothesisLength() == 0);
		assertTrue(a.numDeletions == 0);
		assertTrue(a.numInsertions == 0);
		assertTrue(a.numSubstitutions == 0);
		assertTrue(a.getNumCorrect() == 0);
		assertTrue(a.isSentenceCorrect());
        }
		
        {
		auto a = werEval.align(std::vector<std::string>{"b"}, std::vector<std::string>{"a"});
		assertTrue(a.reference==std::vector<std::string>{"B"});
		assertTrue(a.hypothesis==std::vector<std::string>{"A"});
        assertTrue(a.getReferenceLength() == 1);
		assertTrue(a.getHypothesisLength() == 1);
		assertTrue(a.numDeletions == 0);
		assertTrue(a.numInsertions == 0);
		assertTrue(a.numSubstitutions == 1);
		assertTrue(a.getNumCorrect() == 0);
		assertTrue(!a.isSentenceCorrect());
        }
        {
        auto a = werEval.align(std::vector<std::string>{"b"}, std::vector<std::string>{"a","c"});
		assertTrue(a.reference==std::vector<std::string>{"","B"});
		assertTrue(a.hypothesis==std::vector<std::string>{"A","C"});
		assertTrue(a.getReferenceLength() == 1);
		assertTrue(a.getHypothesisLength() == 2);
		assertTrue(a.numDeletions == 0);
		assertTrue(a.numInsertions == 1);
		assertTrue(a.numSubstitutions == 1);
		assertTrue(a.getNumCorrect() == 0);
		assertTrue(!a.isSentenceCorrect());
        }
        {
		auto a = werEval.align(std::vector<std::string>{"b","c"}, std::vector<std::string>{"a","b"});
		assertTrue(a.reference==std::vector<std::string>{"","b","C"});
		assertTrue(a.hypothesis==std::vector<std::string>{"A","b",""});
        assertTrue(a.getReferenceLength() == 2);
		assertTrue(a.getHypothesisLength() == 2);
		assertTrue(a.numDeletions == 1);
		assertTrue(a.numInsertions == 1);
		assertTrue(a.numSubstitutions == 0);
		assertTrue(a.getNumCorrect() == 1);
		assertTrue(!a.isSentenceCorrect());
        }
		{
		// Reference alignments below created with the NIST sclite tool, default settings
		auto a = werEval.align(split("the quick brown cow jumped over the moon"), split("quick brown cows jumped way over the moon dude"));
		assertTrue(a.reference== std::vector<std::string>{"THE","quick","brown","COW","jumped","","over","the","moon",""});
		assertTrue(a.hypothesis== std::vector<std::string>{"","quick","brown","COWS","jumped","WAY","over","the","moon","DUDE"});
		assertTrue(a.getReferenceLength() == 8);
		assertTrue(a.getHypothesisLength() == 9);
		assertTrue(a.numDeletions == 1);
		assertTrue(a.numInsertions == 2);
		assertTrue(a.numSubstitutions == 1);
		assertTrue(a.getNumCorrect() == 6);
		assertTrue(!a.isSentenceCorrect());
        }
        {
		auto a = werEval.align(split("the quick brown cow jumped over the moon"), split("quick brown over the moon dude"));
		assertTrue(a.reference== std::vector<std::string>{"THE","quick","brown","COW","JUMPED","over","the","moon",""});
		assertTrue(a.hypothesis== std::vector<std::string>{"","quick","brown","","","over","the","moon","DUDE"});
		assertTrue(a.getReferenceLength() == 8);
		assertTrue(a.getHypothesisLength() == 6);
		assertTrue(a.numDeletions == 3);
		assertTrue(a.numInsertions == 1);
		assertTrue(a.numSubstitutions == 0);
		assertTrue(a.getNumCorrect() == 5);
		assertTrue(!a.isSentenceCorrect());
        }
        {
		auto a = werEval.align(split("0 1 1 0 0 1 0 0 0 1 1 1 0 0 0 0 1 1 1 0 1 1 0 1 0 1 0 1 1 1 0 0 0 0 1 1 1 0 0 1 0 1 1 0 1 0 1 1 0 1 0 0 0 1 0 1 0 0 1 0 1 0 1 1 1 0 1 1 0 0 1 1 1 0 0 1 1 1 0 0 1 0 1 1 1 1 1 0 1 0 1 0 0 1 0 0 1 1 0 1"), split("1 0 1 1 0 1 1 1 1 0 0 0 0 0 1 0 0 0 1 1 0 0 1 1 1 0 0 0 0 0 1 0 0 1 0 1 1 0 0 1 1 0 0 1 1 1 1 0 0 0 1 0 1 1 0 0 1 1 1 0 1 0 0 0 1 0 0 1 1 0 0 1 0 1 1 1 1 1 0 0 1 0 0 0 1 1 1 0 1 1 1 0 1 0 1 1 1 0 0 1"));
		assertTrue(a.getReferenceLength() == 100);
		assertTrue(a.getHypothesisLength() == 100);
		assertTrue(a.numDeletions == 10);
		assertTrue(a.numInsertions == 10);
		assertTrue(a.numSubstitutions == 11);
		assertTrue(a.getNumCorrect() == 79);
		assertTrue(!a.isSentenceCorrect());
        }
	}
    public:
    void run() const
    {
        this->testAlignListOfStringListOfString();
        this->testAlignStringArrayStringArray();
        std::cerr << log.str()<<std::endl;
        std::cerr << failed<<"/"<<(passed+failed)<<" tests failed"<<std::endl;
    }
};
