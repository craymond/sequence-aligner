#include "SequenceAligner.hpp"
//#include "TestSequenceAligner.hpp"
#include <iostream>
#include <iterator>
#include <algorithm>
#include <vector>
#include <string>

class Integer
{
	int _i;
	public:
	Integer(): _i(std::numeric_limits<int>::max()) {} //Default constructor should mark the object as empty in some way
	Integer(const int i): _i(i){}
	int size() const {return std::to_string(_i).length();} //size() should provide the nbsymbols to write object on screen
	bool empty() const {return _i==std::numeric_limits<int>::max();}//empty() should say if the object is not empty (not constructed by the constructor without arg)
	bool operator==(const Integer& g) const {return _i==g._i;} //must provide a comparator
	int operator-(const Integer& g) const {return std::min(_i,g._i)*100/std::max(_i,g._i);} //to privilegiate substitution error instead of insertion/deletion when operator== say false but objects share similarity (return 0; to ignore this function), otherwise return the percentage of similarity
	friend std::ostream& operator<<(std::ostream& o,const Integer& i) 
	{
		if(i._i==std::numeric_limits<int>::max()) return o<<"";
		return o<<i._i;
	}
};

void batch(const unsigned int size)
{
	using sentence = std::vector<std::string>;
	sentence sentence1  = {"the", "quick", "brown", "cow", "jumped", "over", "the", "moon"};
	sentence sentence2  = {"quick", "brown", "cow", "jumped", "over", "the", "moon","dude"};
	
	SequenceAligner	batchEval(false);
	std::vector<sentence> batch1(size,sentence1);
	std::vector<sentence> batch2(size,sentence2); 
	auto alignments = batchEval.align(batch1,batch2);
	
	const SequenceAligner<std::string>::SummaryStatistics ss(alignments);
	std::cout<<"\n\n"<<ss<<std::endl;
}

int main(int argc,char*argv[0])
{
	if(argc<2) throw std::invalid_argument("size of batch to test missing");

	//batch(std::atoi(argv[1]));
	//return 0;
	
	std::vector<std::string> sentence1={"the", "quick", "brown", "cow", "jumped", "over", "moon"};
	std::vector<std::string> sentence2={"the","quick", "brown", "cows", "jumped", "way", "over", "the", "moon", "dude"};
	std::vector<std::string> sentence3={"Pepin","de", "Landen"};
	std::vector<std::string> sentence4={"pepin","de", "landen","le","premier"};
	const bool case_sensitive=false;
	SequenceAligner	werEval(case_sensitive);
	std::cout<<"\n----------SINGLE ALIGNMENT--------\n";
	//align 2 sentences					
	auto sentence_alignement = werEval.align(sentence3,sentence4);
	//print stats and alignment
	std::cout<<sentence_alignement<<"\n";

	//get the 2 sentences aligned
	std::cout<<"\nREF=[";
	std::copy(sentence_alignement.reference.cbegin(),sentence_alignement.reference.cend(),std::ostream_iterator<std::string>(std::cout,"\t"));
	std::cout<<"]\nHYP=[";
	std::copy(sentence_alignement.hypothesis.cbegin(),sentence_alignement.hypothesis.cend(),std::ostream_iterator<std::string>(std::cout,"\t"));
    std::cout<<"]";
	
	std::cout<<"\n----------BATCH ALIGNMENT---------\n";
	//align batch of sentences pair
	auto batch_alignement1 = werEval.align({sentence1,sentence2},{sentence3, sentence4});
	//get metrics about batch of alignements
	const SequenceAligner<std::string>::SummaryStatistics ss(batch_alignement1);
	std::cout<<"\n\n"<<ss<<std::endl;
	

	std::vector<Integer> num1={400,2,3,300,308,306};
	std::vector<Integer> num2={2,3,400};

	SequenceAligner<Integer> numEval;
	std::cout<<"\n----------NUMERIC ALIGNMENT--------\n";
	//align 2 sentences					
	auto numeric_alignement = numEval.align(num1,num2);
	//print stats and alignment
	std::cout<<numeric_alignement<<"\n";

	//systematic tests
	//TestSequenceAligner test;
	//test.run();
	
	return EXIT_SUCCESS;
}

