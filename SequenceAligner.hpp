#pragma once
#include <vector>
#include <string>
#include <list>
#include <exception>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <cctype>
#include <memory>
#include "progress-bar.hpp"
#include "asciitablestream.hpp"

int operator-(const std::string& one,const std::string& two);


template<class T=std::string>
class SequenceAligner 
{
	public:
	using mint=unsigned int;
	using cmint=const unsigned  int;
	using sequenceSymboles=std::vector<T>;
	/** Cost of a substitution string edit operation applied during alignment. 
	 * From edu.cmu.sphinx.util.NISTAlign, which should be referencing the NIST sclite utility settings. */
	constexpr static cmint DEFAULT_SUBSTITUTION_PENALTY = 100;
	/** Cost of an insertion string edit operation applied during alignment. 
	 * From edu.cmu.sphinx.util.NISTAlign, which should be referencing the NIST sclite utility settings. */
	constexpr static cmint DEFAULT_INSERTION_PENALTY = 75;
	/** Cost of a deletion string edit operation applied during alignment. 
	 * From edu.cmu.sphinx.util.NISTAlign, which should be referencing the NIST sclite utility settings. */
	constexpr static cmint DEFAULT_DELETION_PENALTY = 75;
	
	private:
	/** case sensitive*/
	const bool case_sensitive;
	/** number of threads*/
	cmint numThreads;
	/** Substitution penalty for reference-hypothesis string alignment */
	cmint substitutionPenalty;		 
	/** Insertion penalty for reference-hypothesis string alignment */
	cmint insertionPenalty;
    /** Deletion penalty for reference-hypothesis string alignment */
	cmint deletionPenalty;
	
	
	static std::string toLowerCase(std::string s)
	{
		std::transform(s.cbegin(), s.cend(), s.begin(), ::tolower);
		return s;
	}
	static std::string toUpperCase(std::string s)
	{
		std::transform(s.cbegin(), s.cend(), s.begin(), ::toupper);
		return s;
	}
	
	static bool equal(const T& s1,const T& s2,const bool case_sensitive)
	{
		if constexpr(std::is_same<T,std::string>::value) 
		{
			if(case_sensitive)
				return s1==s2;
			return toLowerCase(s1)==toLowerCase(s2);
		}
		else return s1==s2;
	}
	
	/**
	 * Result of an alignment.
	 * Has a {@link #toString()} method that pretty-prints human-readable metrics.
	 *  
	 * @author Christian Raymond
	 */
	public:
	struct Alignment 
	{
		/** Reference words, with null elements representing insertions in the hypothesis sentence and upper-cased words representing an alignment mismatch */
		const sequenceSymboles reference;
		/** Hypothesis words, with null elements representing deletions (missing words) in the hypothesis sentence and upper-cased words representing an alignment mismatch */
		const sequenceSymboles hypothesis;		
		/** Number of word substitutions made in the hypothesis with respect to the reference */
		cmint numSubstitutions;
		/** Number of word insertions (unnecessary words present) in the hypothesis with respect to the reference */
		cmint numInsertions;
		/** Number of word deletions (necessary words missing) in the hypothesis with respect to the reference */
		cmint numDeletions;
		/**
		 * Constructor.
		 * @param reference reference words, with null elements representing insertions in the hypothesis sentence
		 * @param hypothesis hypothesis words, with null elements representing deletions (missing words) in the hypothesis sentence
		 * @param numSubstitutions Number of word substitutions made in the hypothesis with respect to the reference 
		 * @param numInsertions Number of word insertions (unnecessary words present) in the hypothesis with respect to the reference
		 * @param numDeletions Number of word deletions (necessary words missing) in the hypothesis with respect to the reference
		 */
		Alignment(const sequenceSymboles& ref, const sequenceSymboles& hyp, cmint numSub, cmint numIns, cmint numDel):	reference(ref),hypothesis(hyp),numSubstitutions(numSub),numInsertions(numIns),numDeletions(numDel)
		{
			if(reference.size() != hypothesis.size() ) 
				throw std::runtime_error("Bad Alignment");			
		}
		
		/**
		 * Number of word correct words in the aligned hypothesis with respect to the reference.
		 * @return number of word correct words 
		 */
		mint getNumCorrect() const {return getHypothesisLength() - (numSubstitutions + numInsertions); } // Substitutions are mismatched and not correct, insertions are extra words that aren't correct		
		
		/** @return true when the hypothesis exactly matches the reference */
		bool isSentenceCorrect() const { return numSubstitutions == 0 && numInsertions == 0 && numDeletions == 0;}
		
		mint getReferenceLength() const{return reference.size() - numInsertions;}

		mint getHypothesisLength() const{return hypothesis.size() - numDeletions; }

		float wer() const noexcept { if(getReferenceLength()>0) return (numSubstitutions + numInsertions + numDeletions) *100.0f / getReferenceLength(); else return numInsertions*100.0f;}

		float accuracy() const noexcept { if(getReferenceLength()>0) return (getNumCorrect() *100.0f / getReferenceLength()); return 0.0f;}

		float precision() const noexcept {if(getHypothesisLength()>0) return getNumCorrect()*100.0f/getHypothesisLength(); return 0;}

		float recall() const noexcept {if(getReferenceLength()>0) return getNumCorrect()*100.0f/getReferenceLength(); return 0;}

		float f1() const noexcept {const float den=precision()+recall(); if(den>0) return 2.0f*precision()*recall()/den; return 0;}

		friend std::ostream& operator<<(std::ostream& o,const SequenceAligner::Alignment& align) 
		{
			asciitablestream sob(asciitablestream::Alignment::LEFT,asciitablestream::Style::EMPTY);
			sob << "REF:";
			for(const auto& e: align.reference)
				sob << e;
			sob<<asciitablestream::endl<<"HYP:";
			for(const auto& e: align.hypothesis)
				sob << e;
			sob<<asciitablestream::endl;
						
			asciitablestream sb(asciitablestream::Alignment::LEFT);
			sb<<""<<"#seq"<<"#ref"<<"#hyp"<<"#cor"<<"#sub"<<"#ins"<<"#del"<<"%acc"<<"%WER"<<"#seq_cor"<<asciitablestream::endl;
			sb<<"STATS"<<1;
			sb<<align.getReferenceLength();
			sb<<align.getHypothesisLength();
			sb<<align.getNumCorrect();
			sb<<align.numSubstitutions;
			sb<<align.numInsertions;
			sb<<align.numDeletions;
			sb<<align.accuracy();
			sb<<align.wer();
			if(align.isSentenceCorrect())
				sb<< 1;
			else 
				sb<< 0;

			sb<<asciitablestream::endl;
						
			return o<< sb<<"\n"<<sob;
		}
	};

	class SummaryStatistics 
	{
		/** Number of correct words in the aligned hypothesis with respect to the reference */
		mint numCorrect=0;
		/** Number of word substitutions made in the hypothesis with respect to the reference */
		mint numSubstitutions=0;
		/** Number of word insertions (unnecessary words present) in the hypothesis with respect to the reference */
		mint numInsertions=0;
		/** Number of word deletions (necessary words missing) in the hypothesis with respect to the reference */
		mint numDeletions=0;
		/** Number of hypotheses that exactly match the associated reference */
		mint numSentenceCorrect=0;
		/** Total number of words in the reference sequences */
		mint numReferenceWords=0;
		/** Total number of words in the hypothesis sequences */
		mint numHypothesisWords=0;
		/** Number of sentences */
		mint numSentences=0;
		/** total number of sentences containing one error of this type **/
		mint numDeletionsSentence=0;
		mint numInsertionsSentence=0;
		mint numSubstitutionsSentence=0;
		
		public:
		/**
		 * Constructor.
		 * @param alignments collection of alignments
		 */
		explicit SummaryStatistics(const std::vector<Alignment>& alignments) 
		{
			for(const auto& a : alignments) 
				add(a);			
		}
		
		/**
		 * Add a new alignment result
		 * @param alignment result to add
		 */
		void add(const Alignment& alignment) {
			numCorrect += alignment.getNumCorrect();
			numSubstitutions += alignment.numSubstitutions;
			numInsertions += alignment.numInsertions;
			numDeletions += alignment.numDeletions;
			numSentenceCorrect += alignment.isSentenceCorrect() ? 1 : 0;
			numReferenceWords += alignment.getReferenceLength();
			numHypothesisWords += alignment.getHypothesisLength();
			numDeletionsSentence +=alignment.numDeletions>0 ? 1 : 0;
		 	numInsertionsSentence +=alignment.numInsertions>0 ? 1 : 0;
		 	numSubstitutionsSentence +=alignment.numSubstitutions>0 ? 1 : 0;
			numSentences++;
		}
		
		int getNumSentences() const noexcept{return numSentences;}
		int getNumReferenceWords() const noexcept{return numReferenceWords;	}
		int getNumHypothesisWords() const noexcept{	return numHypothesisWords;}
		float getCorrectRate() const {return numCorrect*100.0 / numReferenceWords;}
		float getSubstitutionRate() const{return numSubstitutions*100.0 / numReferenceWords;}
		float getDeletionRate() const{return numDeletions*100.0 / numReferenceWords;}
		float getInsertionRate() const{	return numInsertions*100.0 / numReferenceWords;}
		float getSubstitutionSentenceRate() const{return numSubstitutionsSentence*100.0 / numSentences;}
		float getDeletionSentenceRate() const{return numDeletionsSentence*100.0 / numSentences;}
		float getInsertionSentenceRate() const{	return numInsertionsSentence*100.0 / numSentences;}
		/** @return the word error rate of this collection */
		float getWordErrorRate() const{	return (numSubstitutions + numDeletions + numInsertions)*100.0 / numReferenceWords;}
		/** @return the sentence error rate of this collection */
		float getSentenceErrorRate() const{	return (numSentences - numSentenceCorrect)*100.0 / numSentences;}
		int getSentenceError() const noexcept {	return numSentences - numSentenceCorrect;}
		friend std::ostream& operator<<(std::ostream& sob,const SummaryStatistics& ss) 
		{
			asciitablestream sent(asciitablestream::Alignment::RIGHT,asciitablestream::Style::EMPTY,2u,false);
			sent<<"SENTENCE RECOGNITION PERF"<<"#sent="<<ss.numSentences<<asciitablestream::endl;
			sent<<"Error"<<"number"<<"perc."<<asciitablestream::endl;
			sent<<"With errors"<<ss.getSentenceError()<<ss.getSentenceErrorRate()<<asciitablestream::endl;
			sent<<"With substitutions"<<ss.numSubstitutionsSentence<<ss.getSubstitutionSentenceRate()<<asciitablestream::endl;
			sent<<"With deletions"<<ss.numDeletionsSentence<<ss.getDeletionSentenceRate()<<asciitablestream::endl;
			sent<<"With insertions"<<ss.numInsertionsSentence<<ss.getInsertionSentenceRate()<<asciitablestream::endl;
			
			asciitablestream word(asciitablestream::Alignment::RIGHT,asciitablestream::Style::EMPTY,2u,false);
			word<<"WORD RECOGNITION PERF"<<"Ref:"+std::to_string(ss.numReferenceWords)<<"Hyp:"+std::to_string(ss.getNumHypothesisWords())<<asciitablestream::endl;
			word<<"Error"<<"number"<<"perc."<<asciitablestream::endl;
			word<<"Total Error"<<(ss.numSubstitutions + ss.numDeletions + ss.numInsertions)<<ss.getWordErrorRate() <<asciitablestream::endl;			
			word<<"Substitutions"<<ss.numSubstitutions<<ss.getSubstitutionRate()<<asciitablestream::endl;
			word<<"Deletions"<<ss.numDeletions<<ss.getDeletionRate()<<asciitablestream::endl;
			word<<"Insertions"<<ss.numInsertions<<ss.getInsertionRate()<<asciitablestream::endl;
			word<<"Correct"<<ss.numCorrect<<ss.getCorrectRate()<<asciitablestream::endl;
			//return sob << sent << word;
			return sob << sent.toStringBeside(word,"\t|\t");
		}
	};	

	/**
	 * Constructor. 
	 * @param cs case sensitive matching
	 * @param n_jobs number of threads to use
	 * @param substitutionPenalty substitution penalty for reference-hypothesis string alignment
	 * @param insertionPenalty insertion penalty for reference-hypothesis string alignment
	 * @param deletionPenalty deletion penalty for reference-hypothesis string alignment
	 */
	SequenceAligner(const bool cs=false,cmint n_jobs=1,cmint substitutionP=DEFAULT_SUBSTITUTION_PENALTY, cmint insertionP=DEFAULT_INSERTION_PENALTY, cmint deletionP=DEFAULT_DELETION_PENALTY) : case_sensitive(cs), numThreads(n_jobs), substitutionPenalty(substitutionP), insertionPenalty(insertionP), deletionPenalty(deletionP){}	
	
	/**
	 * Produces {@link Alignment} results from the alignment of the hypothesis words to the reference words.
	 * Alignment is done via weighted string edit distance according to {@link #substitutionPenalty}, {@link #insertionPenalty}, {@link #deletionPenalty}.
	 * 
	 * @param reference sequence of words representing the true sentence; will be evaluated as lowercase.
	 * @param hypothesis sequence of words representing the hypothesized sentence; will be evaluated as lowercase.
	 * @return results of aligning the hypothesis to the reference 
	 */
	Alignment align(const sequenceSymboles& reference, const sequenceSymboles& hypothesis) const
	{
		// Values representing string edit operations in the backtrace matrix
		//enum EditOperation{OK = 0,SUB = 1, INS = 2,DEL = 3};
		constexpr mint OK=0;
		constexpr mint SUB=1;
		constexpr mint INS=2;
		constexpr mint DEL=3;
		/* 
		 * Next up is our dynamic programming tables that track the string edit distance calculation.
		 * The row address corresponds to an index within the sequence of reference words.
		 * The column address corresponds to an index within the sequence of hypothesis words.
		 * cost[0][0] addresses the beginning of two word sequences, and thus always has a cost of zero.  
		 */
		
		/** cost[3][2] is the minimum alignment cost when aligning the first two words of the reference to the first word of the hypothesis */
		std::vector<std::vector<mint>> cost(reference.size() + 1,std::vector<mint>(hypothesis.size() + 1,0));

		/** 
		 * backtrace[3][2] gives information about the string edit operation that produced the minimum cost alignment between the first two words of the reference to the first word of the hypothesis.
		 * If a deletion operation is the minimum cost operation, then we say that the best way to get to hyp[1] is by deleting ref[2].
		 */
		std::vector<std::vector<mint>> backtrace(reference.size() + 1,std::vector<mint>(hypothesis.size() + 1,0));
		// Initialization
		cost[0][0] = 0;
		backtrace[0][0] = OK;
		
		// First column represents the case where we achieve zero hypothesis words by deleting all reference words.
		for(mint i=1; i<cost.size(); ++i) 
		{
			cost[i][0] = deletionPenalty * i;
			backtrace[i][0] = DEL; 
		}
		
		// First row represents the case where we achieve the hypothesis by inserting all hypothesis words into a zero-length reference.
		for(mint j=1; j<cost[0].size(); ++j) 
		{
			cost[0][j] = insertionPenalty * j;
			backtrace[0][j] = INS; 
		}

		// For each next column, go down the rows, recording the min cost edit operation (and the cumulative cost). 
		for(mint i=1; i<cost.size(); ++i) 
		{
			for(mint j=1; j<cost[0].size(); ++j) 
			{
				mint subOp, cs;  // it is a substitution if the words aren't equal, but if they are, no penalty is assigned.
				if(equal(reference[i-1],hypothesis[j-1],this->case_sensitive)) //compare using tolower or not
				{
					subOp = OK;
					cs = cost[i-1][j-1];
				} 
				else 
				{
					subOp = SUB;
					mint discountCost=abs(reference[i-1]-hypothesis[j-1]);
					if(discountCost>0) discountCost = substitutionPenalty*discountCost/100;					
					cs = cost[i-1][j-1] + substitutionPenalty - discountCost ;
				}
				cmint ci = cost[i][j-1] + insertionPenalty;
				cmint cd = cost[i-1][j] + deletionPenalty;
				
				cmint mincost = std::min(cs, std::min(ci, cd));
				if(cs == mincost) 
				{
					cost[i][j] = cs;
					backtrace[i][j] = subOp;
				}
				else 
				{
					if(ci == mincost) 
					{
						cost[i][j] = ci;
						backtrace[i][j] = INS;					
					} 
					else 
					{
						cost[i][j] = cd;
						backtrace[i][j] = DEL;					
					}
				}
			}
		}
		
		// Now that we have the minimal costs, find the lowest cost edit to create the hypothesis sequence
		std::list<T> alignedReference;
		std::list<T> alignedHypothesis;
		mint numSub = 0;
		mint numDel = 0;
		mint numIns = 0;
		int i = cost.size() - 1;
		int j = cost[0].size() - 1;
		while(i > 0 || j > 0) 
		{
			switch(backtrace[i][j]) 
			{
			case OK:  alignedReference.push_front(reference[i-1]); //toLowerCase pour identifier les mots correctement reconnus et toUpper pour les mots mal reconnus
					  alignedHypothesis.push_front(hypothesis[j-1]); i--; j--; break;
			case SUB: alignedReference.push_front(reference[i-1]); 
					  alignedHypothesis.push_front(hypothesis[j-1]); i--; j--; numSub++; break;
			case INS: alignedReference.push_front({}); 
				      alignedHypothesis.push_front(hypothesis[j-1]); j--; numIns++; break;
			case DEL: alignedReference.push_front(reference[i-1]); 
					  alignedHypothesis.push_front({}); i--; numDel++; break;
			}
		}
	
		return Alignment({alignedReference.begin(),alignedReference.end()}, {alignedHypothesis.begin(),alignedHypothesis.end()}, numSub, numIns, numDel);
	}

/**
	 * Produce alignment results for several pairs of sentences.
	 * @see #align(String[], String[])
	 * @param references reference sentences to align with the given hypotheses 
	 * @param hypotheses hypothesis sentences to align with the given references
	 * @return collection of per-sentence alignment results
	 */
	std::vector<Alignment> align(const std::vector<sequenceSymboles>& references, const std::vector<sequenceSymboles>& hypotheses) const {
		if(references.size() != hypotheses.size()) throw std::runtime_error("Illegal argument");
		
		if(references.empty()) 	return std::vector<Alignment>();		
		
		std::vector<std::unique_ptr<Alignment>> alignments(references.size());
		
		progressbar::cfg  pcfg;
		pcfg.style=progressbar::TQDM;
		pcfg.bar_size=50;
		progressbar b(references.size(),"Align sentences:",pcfg);
		#pragma omp parallel for num_threads(numThreads)
		for(mint i=0;i<references.size();++i) 
		{
			alignments[i]=std::make_unique<Alignment>(align(references[i], hypotheses[i]));
			#pragma omp critical
			std::cerr<<++b;
		}
		
		
		std::vector<Alignment> valignments;
		for(auto& e: alignments)
			valignments.push_back(std::move(*e));

		return valignments;
	}
};

//utility for string 

class Char
{
	char _c;
	public:
	Char(): _c(0) {} //Default constructor should mark the object as empty in some way
	Char(const char c): _c(c){}
	int size() const {return 1;} //size() should provide the nbsymbols to write object on screen
	bool empty() const {return _c==0;}//empty() should say if the object is not empty (not constructed by the constructor without arg)
	bool operator==(const Char& g) const {return _c==g._c;} //must provide a comparator
	int operator-(const Char& g) const {return 0;} //to privilegiate substitution error instead of insertion/deletion when operator== say false but objects share similarity (return 0; to ignore this function)
	friend std::ostream& operator<<(std::ostream& o,const Char& i) 
	{
		if(i._c==0) return o<<"";
		return o<<i._c;
	}
};


inline int operator-(const std::string& one,const std::string& two)
{
	SequenceAligner<Char> wordAligner(false);
	std::vector<Char> un{one.begin(),one.end()};
	std::vector<Char> deux{two.begin(),two.end()};
	const auto& res=wordAligner.align(un ,deux);
	return res.f1();
}
